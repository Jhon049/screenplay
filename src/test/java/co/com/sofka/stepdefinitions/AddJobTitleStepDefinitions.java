package co.com.sofka.stepdefinitions;

import co.com.sofka.model.Job;
import co.com.sofka.util.Admin;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import javax.swing.*;

import static co.com.sofka.question.jobtitle.CreateSuccess.SuccessfullySaved;
import static co.com.sofka.task.homepage.GoToJobTitle.goToJobTitlePage;
import static co.com.sofka.task.jobtitlepage.CreateJob.createANewJob;
import static co.com.sofka.task.landingpage.OpenLandingPage.openLandingPage;
import static co.com.sofka.util.Dictionary.*;
import static co.com.sofka.util.JobCreate.generateJob;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static co.com.sofka.task.landingpage.FillLogIn.fillLogIn;

public class AddJobTitleStepDefinitions extends Setup{

    private final Logger LOGGER = Logger.getLogger(AddJobTitleStepDefinitions.class);
    private final String ACTOR_NAME = "Pepito";
    private Job newJob;

    @Given("que el usuario administrativo ingreso a la seccion job titles")
    public void queElUsuarioAdministrativoIngresoALaSeccionJobTitles(){
        try{
            actorSetupTheBrowser(ACTOR_NAME);
            theActorInTheSpotlight().wasAbleTo(
                    openLandingPage(),
                    fillLogIn()
                            .withUser(Admin.USER.getValue())
                            .andPassword(Admin.PASSWORD.getValue()),
                    goToJobTitlePage()
            );
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @When("el usuario adiciona un job title")
    public void elUsuarioAdicionaUnJobTitle(){
        try {
            newJob = generateJob(SPANISH_CODE_LANGUAGE.getValue(), COUNTRY_CODE.getValue());
            theActorInTheSpotlight().attemptsTo(
                    createANewJob()
                            .withJobTitle(newJob.getJobTitle())
                            .withJobDescription(newJob.getJobDescription())
                            .withJobSpecificationFile(JOB_SPECIFICATION_FILE.getValue())
                            .andJobNotes(newJob.getNote())
            );
        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Then("el sistema confirmara la creacion del job title")
    public void elSistemaConfirmaraLaCreacionDelJobTitle(){
        try {
            theActorInTheSpotlight().should(
                    seeThat(SuccessfullySaved())
            );
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
