package co.com.sofka.task.landingpage;

import co.com.sofka.userinterface.landingpage.LandingPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

public class FillLogIn implements Task {
    private String user;
    private String password;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(LandingPage.USER_FIELD),
                Click.on(LandingPage.USER_FIELD),
                Enter.theValue(user).into(LandingPage.USER_FIELD),

                Scroll.to(LandingPage.PASSWORD_FIELD),
                Click.on(LandingPage.PASSWORD_FIELD),
                Enter.theValue(password).into(LandingPage.PASSWORD_FIELD),

                Scroll.to(LandingPage.LOGIN_BUTTON),
                Click.on(LandingPage.LOGIN_BUTTON)
        );
    }
    public FillLogIn withUser(String username) {
        this.user = username;
        return this;
    }
    public FillLogIn andPassword(String password) {
        this.password = password;
        return this;
    }
    public static FillLogIn fillLogIn(){
        return new FillLogIn();
    }
}
