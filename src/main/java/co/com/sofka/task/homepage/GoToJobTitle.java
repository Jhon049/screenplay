package co.com.sofka.task.homepage;

import co.com.sofka.userinterface.home.HomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.HoverOverTarget;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.actions.Scroll;

public class GoToJobTitle implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(HomePage.ADMIN_BTN),
                Click.on(HomePage.ADMIN_BTN),
                HoverOverTarget.over(HomePage.ADMIN_BTN),

                MoveMouse.to(HomePage.JOB_MENU),
                Click.on(HomePage.JOB_MENU),

                HoverOverTarget.over(HomePage.JOB_TITLE_VIEW),
                MoveMouse.to(HomePage.JOB_TITLE_VIEW),
                Click.on(HomePage.JOB_TITLE_VIEW)

        );
    }
    public static GoToJobTitle goToJobTitlePage(){
        return new GoToJobTitle();
    }
}
