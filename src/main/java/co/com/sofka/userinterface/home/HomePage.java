package co.com.sofka.userinterface.home;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomePage {

    public static final Target ADMIN_BTN = Target
            .the("Admin")
            .located(By.id("menu_admin_viewAdminModule"));

    public static final Target JOB_MENU = Target
            .the("job tab")
            .located(By.id("menu_admin_Job"));

    public static final Target JOB_TITLE_VIEW = Target
            .the("job title link")
            .located(By.id("menu_admin_viewJobTitleList"));
}
