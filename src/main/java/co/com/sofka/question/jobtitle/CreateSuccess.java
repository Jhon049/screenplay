package co.com.sofka.question.jobtitle;

import co.com.sofka.userinterface.jobtitle.JobTitlePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CreateSuccess implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        return JobTitlePage.JOB_CREATED_MESSAGE.resolveFor(actor).isCurrentlyVisible();
    }

    public static CreateSuccess SuccessfullySaved(){
        return new CreateSuccess();
    }
}
