package co.com.sofka.util;

import com.github.javafaker.Faker;

import co.com.sofka.model.Job;
import java.util.Locale;
import java.util.Random;

public class JobCreate {

        public static Job generateJob(String language, String country) {

                Faker faker = Faker.instance(
                        new Locale(language, country),
                        new Random()
                );

                Job newJob = new Job();
                newJob.setJobTitle(faker.job().title());
                newJob.setJobDescription(faker.job().field());
                newJob.setNote(faker.shakespeare().hamletQuote());
                return newJob;
        }


}
