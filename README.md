# sCREENPLAY

# Plan de Pruebas ScreenPlay

Se realiza plan de pruebas para verificación del módulo **Job Title** del sitio web [https://opensource-demo.orangehrmlive.com/](https://opensource-demo.orangehrmlive.com/)

- **Alcance de Pruebas**
    
    El alcance de las pruebas se determina según cada HU.
    
    - **HU001 - Job Title**
        - Alcance de pruebas
            - Añadir un Titulo de trabajo
        - Fuera de Alcance
            - No se validará más de un Job Title
            - No se validará la creación con únicamente datos obligatorios
            - No se validarán módulos diferentes
            - No se validará en diferentes navegadores web
- **Estrategia de Pruebas**
    
    Realizadas bajo el patrón ScreenPlay, a través de 1 escenario determinado por la Historia de Usuario determinando el funcionamiento a través del uso del modulo con actor determinado.
    
- **Cronograma de Entrega**
    
    Se realiza la entrega de la prueba automatizada funcionando verificado dentro del plazo establecido.
    
- **Otros Tipos de Pruebas**
    
    Se recomienda al equipo realizar las siguientes pruebas:
    
    - Pruebas de Rendimiento
    - Pruebas de Seguridad
    - Pruebas de Mantenimiento
- **Requerimientos**
    
    Se requiere acceso a la web propiedad de tercero, las credenciales son públicas. La disponibilidad de la web no es constante.
    
- **Acuerdos**
    
    Se debe garantizar la ejecución de las pruebas en cualquier equipo siempre que cuente con el software requerido.
    
    La prueba automatizada debe ser repetible.
