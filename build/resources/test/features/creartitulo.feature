Feature: Creacion de titulo de trabajo
  Como usuario administrativo
  deseo agregar un titulo de trabajo
  con el fin de actualizar la informacion en sistema

  Scenario: Creacion de titulo de trabajo
    Given que el usuario administrativo ingreso a la seccion job titles
    When el usuario adiciona un job title
    Then el sistema confirmara la creacion del job title